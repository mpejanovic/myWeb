var app = angular.module('Bioskop', ['ngRoute']);

app.controller('Ctrl', function($scope){
	$scope.message = "Dobrodošli!"
});

app.controller('filmoviCtrl', function($scope, $http, $location){
	var baseUrlFilmovi = "/api/filmovi";
	var baseUrlBioskopi = "/api/bioskopi";
	var baseUrlZanrovi = "/api/zanrovi";
	$scope.filmovi = [];
	$scope.bioskopi = [];
	$scope.zanrovi = [];
	
	$scope.pPretrage = {};
	$scope.pPretrage.naziv = "";
	$scope.pPretrage.min = "";
	$scope.pPretrage.max = "";
	$scope.pPretrage.bioskopId = "";
	$scope.pPretrage.zanrId = "";
	
	$scope.totalPages = 1;
	$scope.pageNum = 0;
	
	$scope.stanje = "";
	
	var toggled = true;
	
	$scope.toggle = function(){
		if(toggled){
			$scope.stanje = "nestalo";
			getFilmovi();
		} else {
			$scope.stanje = "";
			getFilmovi();
		}
		toggled = !toggled;
	}
	
	var getFilmovi = function () {
		var config = { params : {}};
		if($scope.pPretrage.naziv != ""){
			config.params.naziv = $scope.pPretrage.naziv;
		}
		if($scope.pPretrage.min != ""){
			config.params.min = $scope.pPretrage.min;
		}
		if($scope.pPretrage.max != ""){
			config.params.max = $scope.pPretrage.max;
		}
		if($scope.pPretrage.bioskopId != ""){
			config.params.bioskopId = $scope.pPretrage.bioskopId;
		}
		if($scope.pPretrage.zanrId != ""){
			config.params.zanrId = $scope.pPretrage.zanrId;
		}
		if($scope.stanje != ""){
			config.params.stanje = 0;
		}
		config.params.page = $scope.pageNum;
		$http.get(baseUrlFilmovi, config).then(
			function success (arg) {
				$scope.filmovi = arg.data;
				$scope.totalPages = arg.headers("totalPages");
			}, function error (arg) {
				alert("Preuzimanje filma nije uspesno")
			})
	}
	getFilmovi();
	
	var getBioskopi = function () {
		$http.get(baseUrlBioskopi).then(
			function success (arg) {
				$scope.bioskopi = arg.data;
			}, function error (arg) {
				alert("Preuzimanje bioskopa nije uspesno")
			})
	}
	getBioskopi();
	
	var getZanrovi = function () {
		$http.get(baseUrlZanrovi).then(
			function success (arg) {
				$scope.zanrovi = arg.data;
			}, function error (arg) {
				alert("Preuzimanje zanrova nije uspesno")
			})
	}
	getZanrovi();
	
	
	$scope.pretrazi = function () {
		getFilmovi();
		$scope.pPretrage.naziv = "";
		$scope.pPretrage.min = "";
		$scope.pPretrage.max = "";
		$scope.pPretrage.bioskopId = "";
		$scope.pPretrage.zanrId = "";
	}
	
	$scope.reset = function(){
		$scope.pPretrage.naziv = "";
		$scope.pPretrage.min = "";
		$scope.pPretrage.max = "";
		$scope.pPretrage.bioskopId = "";
		$scope.pPretrage.zanrId = "";
		getFilmovi();
	}

	$scope.goToDodaj = function () {
		$location.path("/dodaj");
	}

	$scope.obrisi = function (id) {
		$http.delete(baseUrlFilmovi + "/" + id).then(
			function success (arg) {
				getFilmovi();
			}, function error (arg) {
				alert("Brisanje filma nije uspesno")
			})
	}
	$scope.izmeni = function (id) {
		$location.path("/izmeni/" + id);
	}
	
	$scope.go = function (direction) {
		$scope.pageNum = $scope.pageNum + direction;
		getFilmovi();
	}

	$scope.kupi = function(id){
		$http.post(baseUrlFilmovi + "/" + id + "/kupi").then(
			function success () {
				alert("Uspesno kupljena karta");
				getFilmovi();
			}, function error () {
				alert("Nije moguce kupiti kartu");
				getFilmovi();
			})
	}
});


app.controller('izmeniCtrl', function($http, $scope, $routeParams, $location){
	$scope.stariFilm = {};
	$scope.stariFilm.naziv = "";
	$scope.stariFilm.glavniGlumac = "";
	$scope.stariFilm.stanje = "";
	$scope.stariFilm.cena = "";


	var baseUrlFilmovi = "/api/filmovi";

	var filmId = $routeParams.id;
	
	var getStariFilm = function () {
		$http.get(baseUrlFilmovi + "/" + filmId).then(
			function success (res) {
				$scope.stariFilm = res.data;
			}, function error (res) {
				alert("Neuspesno preuzimanje filma");
			})
	}
	getStariFilm();

	
	$scope.izmeni = function () {
		$http.put(baseUrlFilmovi + "/" + filmId, $scope.stariFilm).then(
			function success (res) {
				$location.path("/filmovi")
			}, function error (res) {
				alert("Neuspesna izmena filma");
			})
	}

});

app.controller('addFilmCtrl', function($scope, $http, $location){
	var baseUrlFilmovi = "/api/filmovi";
	$scope.noviFilm = {};
	$scope.noviFilm.naziv = "";
	$scope.noviFilm.glavniGlumac = "";
	$scope.noviFilm.stanje = "";
	$scope.noviFilm.cena = "";
	$scope.noviFilm.bioskopId = "";
	$scope.noviFilm.zanrId = "";
	
	$scope.bioskopi = [];
	var baseUrlBioskopi = "/api/bioskopi";
	var getBioskopi = function () {
		$http.get(baseUrlBioskopi).then(
			function success (arg) {
				$scope.bioskopi = arg.data;
			}, function error (arg) {
				alert("Preuzimanje bioskopa nije uspesno")
			})
	}
	getBioskopi();
	
	$scope.zanrovi = [];
	var baseUrlZanrovi = "/api/zanrovi"
		var getZanrovi = function () {
		$http.get(baseUrlZanrovi).then(
			function success (arg) {
				$scope.zanrovi = arg.data;
			}, function error (arg) {
				alert("Preuzimanje zanrova nije uspesno")
			})
	}
	getZanrovi();
	
	$scope.dodaj = function () {
		$http.post(baseUrlFilmovi, $scope.noviFilm).then(
			function success (res) {
				$location.path("/filmovi");
				getFilmovi();
			}, function error (res) {
				alert("Nije moguće dodati film")
			})
	}

});
app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/partial/home.html',
			controller: 'Ctrl'
		})
		.when('/filmovi', {
			templateUrl : '/app/html/partial/filmovi.html',
			controller: 'filmoviCtrl'
		})
		.when('/dodaj', {
			templateUrl : '/app/html/partial/add-film.html',
			controller: 'addFilmCtrl'
		})
		.when('/izmeni/:id', {
			templateUrl : '/app/html/partial/izmeni-film.html',
			controller: 'izmeniCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);