package jwd.projekat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Film {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private String glavniGlumac;
	@Column
	private Integer stanje;
	@Column
	private Double cena;
	@ManyToOne(fetch=FetchType.EAGER)
	private Bioskop bioskop;
	@ManyToOne(fetch=FetchType.EAGER)
	private Zanr zanr;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public Integer getStanje() {
		return stanje;
	}
	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}
	
	public Bioskop getBioskop() {
		return bioskop;
	}

	public void setBioskop(Bioskop bioskop) {
	this.bioskop = bioskop;
	if(!bioskop.getFilmovi().contains(this))
		bioskop.getFilmovi().add(this);
}
	
	public String getGlavniGlumac() {
		return glavniGlumac;
	}
	public void setGlavniGlumac(String glavniGlumac) {
		this.glavniGlumac = glavniGlumac;
	}
	public Zanr getZanr() {
		return zanr;
	}

	public void setZanr(Zanr zanr) {
	this.zanr = zanr;
	if(!zanr.getFilmovi().contains(this))
		zanr.getFilmovi().add(this);
}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	
	
	
	

	


	
}
