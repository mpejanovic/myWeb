package jwd.projekat.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table
public class Zanr {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@OneToMany(mappedBy="zanr")
	private List<Film> filmovi = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public List<Film> getFilmovi() {
		return filmovi;
	}

	public void addFilm(Film film){
		this.filmovi.add(film);
		
		if(!this.equals(film.getZanr())){
			film.setZanr(this);
		}
	}
	
}
