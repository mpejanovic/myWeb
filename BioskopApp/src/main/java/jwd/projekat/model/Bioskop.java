package jwd.projekat.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table
public class Bioskop {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private String adresa;
	@Column
	private String mesto;
	@OneToMany(mappedBy="bioskop")
	private List<Film> filmovi = new ArrayList<>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getMesto() {
		return mesto;
	}
	public void setMesto(String mesto) {
		this.mesto = mesto;
	}
	public List<Film> getFilmovi() {
		return filmovi;
	}
	public void setFilmovi(List<Film> filmovi) {
		this.filmovi = filmovi;
	}
	public void addFilm(Film film){
		this.filmovi.add(film);
		
		if(!this.equals(film.getBioskop())){
			film.setBioskop(this);
		}
	}
	
	
}
