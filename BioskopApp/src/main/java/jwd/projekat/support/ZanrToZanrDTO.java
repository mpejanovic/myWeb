package jwd.projekat.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.projekat.model.Zanr;
import jwd.projekat.web.dto.ZanrDTO;
@Component
public class ZanrToZanrDTO implements Converter<Zanr, ZanrDTO> {

	@Override
	public ZanrDTO convert(Zanr zanr) {
		ZanrDTO dto = new ZanrDTO();
		dto.setId(zanr.getId());
		dto.setNaziv(zanr.getNaziv());
		return dto;
	}
	
	public List<ZanrDTO> convert(List<Zanr> zanrovi){
		List<ZanrDTO> lista = new ArrayList<>();
		for(Zanr arg: zanrovi) {
			lista.add(convert(arg));
		}
		return lista;
	}

}
