package jwd.projekat.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.projekat.model.Bioskop;
import jwd.projekat.web.dto.BioskopDTO;
@Component
public class BioskopToBioskopDTO implements Converter<Bioskop, BioskopDTO> {

	@Override
	public BioskopDTO convert(Bioskop bioskop) {
		BioskopDTO dto = new BioskopDTO();
		dto.setId(bioskop.getId());
		dto.setNaziv(bioskop.getNaziv());
		dto.setAdresa(bioskop.getAdresa());
		dto.setMesto(bioskop.getMesto());
		return dto;
	}
	
	public List<BioskopDTO> convert(List<Bioskop> args){
		List<BioskopDTO> lista = new ArrayList<>();
		for(Bioskop arg: args) {
			lista.add(convert(arg));
		}
		return lista;
	}

}
