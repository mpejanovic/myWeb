package jwd.projekat.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.projekat.model.Film;
import jwd.projekat.service.BioskopService;
import jwd.projekat.service.ZanrService;
import jwd.projekat.service.FilmService;
import jwd.projekat.web.dto.FilmDTO;
@Component
public class FilmDTOToFilm implements Converter<FilmDTO, Film> {
	
	@Autowired
	FilmService filmService;
	@Autowired
	BioskopService bioskopService;
	
	@Autowired
	ZanrService zanrService;
	
	@Override
	public Film convert(FilmDTO dto) {
		Film film;
		if(dto.getId() == null) {
			film = new Film();
			film.setBioskop(bioskopService.findOne(dto.getBioskopId()));
			film.setZanr(zanrService.findOne(dto.getZanrId()));
		} else {
			film = filmService.findOne(dto.getId());
			if(film == null)
				throw new IllegalArgumentException("Nepostojeci id");
		}
		film.setNaziv(dto.getNaziv());
		film.setCena(dto.getCena());
		film.setGlavniGlumac(dto.getGlavniGlumac());
		film.setStanje(dto.getStanje());
		film.setBioskop(bioskopService.findOne(dto.getBioskopId()));
		film.setZanr(zanrService.findOne(dto.getZanrId()));
		return film;
	}
	
	public List<Film> convert(List<FilmDTO> dtos){
		List<Film> lista = new ArrayList<>();
		for(FilmDTO arg: dtos) {
			lista.add(convert(arg));
		}
		return lista;
	}

}
