package jwd.projekat.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.projekat.model.Film;
import jwd.projekat.web.dto.FilmDTO;
@Component
public class FilmToFilmDTO implements Converter<Film, FilmDTO> {

	@Override
	public FilmDTO convert(Film film) {
		FilmDTO dto = new FilmDTO();
		dto.setId(film.getId());
		dto.setNaziv(film.getNaziv());
		dto.setGlavniGlumac(film.getGlavniGlumac());
		dto.setStanje(film.getStanje());
		dto.setCena(film.getCena());
		dto.setBioskopId(film.getBioskop().getId());
		dto.setBioskopNaziv(film.getBioskop().getNaziv());
		dto.setZanrId(film.getZanr().getId());
		dto.setZanrNaziv(film.getZanr().getNaziv());
		return dto;
	}
	
	public List<FilmDTO> convert(List<Film> args){
		List<FilmDTO> lista = new ArrayList<>();
		for(Film arg: args) {
			lista.add(convert(arg));
		}
		return lista;
	}
}
