package jwd.projekat.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.projekat.model.Film;
@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {
	List<Film> findByBioskopId(Long bioskopId);
	
	List<Film> findByZanrId(Long zanrId);
	
	@Query("SELECT f FROM Film f WHERE "
			+ "(:naziv IS NULL OR f.naziv like :naziv) AND "
			+ "(:min IS NULL OR f.cena >= :min) AND "
			+ "(:max IS NULL OR f.cena <= :max) AND "
			+ "(:bioskopId IS NULL OR f.bioskop.id = :bioskopId) AND "
			+ "(:zanrId IS NULL OR f.zanr.id = :zanrId) AND "
			+ "(:stanje IS NULL OR f.stanje = :stanje)")
	Page<Film> pretraga(@Param("naziv") String naziv, 
			@Param("min") Double min, 
			@Param("max") Double max, 
			@Param("bioskopId") Long bioskopId, 
			@Param("zanrId") Long zanrId,
			@Param("stanje") Integer stanje, 
			Pageable pagable);
}
