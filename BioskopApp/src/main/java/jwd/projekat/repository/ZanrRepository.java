package jwd.projekat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.projekat.model.Zanr;
@Repository
public interface ZanrRepository extends JpaRepository<Zanr, Long> {

}
