package jwd.projekat.service;

import java.util.List;

import jwd.projekat.model.Zanr;


public interface ZanrService {
	List<Zanr> findAll();
	Zanr findOne(Long id);
	Zanr save(Zanr zanr);
	void delete(Long id);
}
