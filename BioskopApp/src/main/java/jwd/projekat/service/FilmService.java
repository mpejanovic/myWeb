package jwd.projekat.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;


import jwd.projekat.model.Film;


public interface FilmService {
	Page<Film> findAll(int page);
	Film findOne(Long id);
	Film save(Film film);
	void delete(Long id);
	
//	Page<Festival> findByMestoId(int pageNum, Long mestoId); PAGINIRANO
	List<Film> findByBioskopId(Long bioskopId);
	List<Film> findByZanrId(Long zanrId);
	Page<Film> pretraga(@Param("naziv") String naziv, @Param("min") Double min, @Param("max") Double max,@Param("bioskopId") Long bioskopId, @Param("zanrId") Long zanrId,@Param("stanje") Integer stanje, int page);
	//Zadatak promeniStanje(Zadatak zadatak);
	Film kupi(Long id);
}
