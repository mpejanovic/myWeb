package jwd.projekat.service;

import java.util.List;

import jwd.projekat.model.Bioskop;

public interface BioskopService {
	
	List<Bioskop> findAll();
	Bioskop findOne(Long id);
	Bioskop save(Bioskop bioskop);
	void delete(Long id);
	
}
