package jwd.projekat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.projekat.model.Zanr;
import jwd.projekat.repository.ZanrRepository;
import jwd.projekat.service.ZanrService;
@Service
public class JpaZanrService implements ZanrService {
	@Autowired
	ZanrRepository zanrRepository;
	@Override
	public List<Zanr> findAll() {
		return zanrRepository.findAll();
	}

	@Override
	public Zanr findOne(Long id) {
		return zanrRepository.findOne(id);
	}

	@Override
	public Zanr save(Zanr zanr) {
		return zanrRepository.save(zanr);
	}

	@Override
	public void delete(Long id) {
		zanrRepository.delete(id);
	}

}
