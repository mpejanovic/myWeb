package jwd.projekat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.projekat.model.Bioskop;
import jwd.projekat.repository.BioskopRepository;
import jwd.projekat.service.BioskopService;
@Service
public class JpBioskopService implements BioskopService {
	
	@Autowired
	BioskopRepository bioskopRepository;
	@Override
	public List<Bioskop> findAll() {
		return bioskopRepository.findAll();
	}

	@Override
	public Bioskop findOne(Long id) {
		return bioskopRepository.findOne(id);
	}

	@Override
	public Bioskop save(Bioskop bioskop) {
		return bioskopRepository.save(bioskop);
	}

	@Override
	public void delete(Long id) {
		bioskopRepository.delete(id);
	}

}
