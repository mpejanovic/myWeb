package jwd.projekat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.projekat.model.Film;
import jwd.projekat.repository.FilmRepository;
import jwd.projekat.repository.ZanrRepository;
import jwd.projekat.service.FilmService;
@Service
public class JpaFilmService implements FilmService {
	@Autowired
	FilmRepository filmRepository;
	@Autowired
	ZanrRepository zanrReposotory;
	@Override
	public Page<Film> findAll(int page) {
		return filmRepository.findAll(new PageRequest(page, 3));
	}

	@Override
	public Film findOne(Long id) {
		return filmRepository.findOne(id);
	}

	@Override
	public Film save(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public void delete(Long id) {
		filmRepository.delete(id);
	}

	@Override
	public List<Film> findByBioskopId(Long bioskopId) {
		return filmRepository.findByBioskopId(bioskopId);
	}
	
	@Override
	public List<Film> findByZanrId(Long zanrId) {
		return filmRepository.findByZanrId(zanrId);
	}

	@Override
	public Page<Film> pretraga(String naziv, Double min, Double max, Long bioskopId, Long zanrId, Integer stanje,int page) {
		if(naziv != null) {
			naziv = "%" + naziv + "%";
		}
		return filmRepository.pretraga(naziv, min, max, bioskopId, zanrId, stanje, new PageRequest(page, 3));
	}


	@Override
	public Film kupi(Long id) {
		Film film = filmRepository.findOne(id);
		if(film != null) {
			Integer stanjeKarata = film.getStanje();
			if(stanjeKarata>0) {
				film.setStanje(film.getStanje()-1);
			}else {
				film = null;
			}
		}
		return film;
	}







}
