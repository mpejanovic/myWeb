package jwd.projekat;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jwd.projekat.model.Bioskop;
import jwd.projekat.model.Zanr;
import jwd.projekat.model.Film;
import jwd.projekat.service.BioskopService;
import jwd.projekat.service.ZanrService;
import jwd.projekat.service.FilmService;

@Component
public class TestData {
	
	@Autowired
	ZanrService zanrService;
	
	@Autowired
	BioskopService bioskopService;
	
	@Autowired
	FilmService filmService;
	
	@PostConstruct
	public void init() {
		//zanrovi
		Zanr z1 = new Zanr();
		z1.setNaziv("Drama");
		zanrService.save(z1);
		
		Zanr z2 = new Zanr();
		z2.setNaziv("Triler");
		zanrService.save(z2);
		
		Zanr z3 = new Zanr();
		z3.setNaziv("Komedija");
		zanrService.save(z3);
		
		//bioskopi
		Bioskop b1 = new Bioskop();
		b1.setNaziv("Cinema");
		b1.setAdresa("Kralja Aleksandra");
		b1.setMesto("Zrenjanin");
		bioskopService.save(b1);
		
		Bioskop b2 = new Bioskop();
		b2.setNaziv("Arena Cineplex");
		b2.setAdresa("Bulevar Mihajla Pupina");
		b2.setMesto("Novi Sad");
		bioskopService.save(b2);
		
		
		//filmovi
		Film f1 = new Film();
		f1.setNaziv("Tajna u njihovim ocima");
		f1.setGlavniGlumac("Soledad Villamil");
		f1.setStanje(10);
		f1.setCena(350.0);
		f1.setZanr(z2);
		f1.setBioskop(b1);
		filmService.save(f1);
		
		Film f2 = new Film();
		f2.setNaziv("Frka na vencanju");
		f2.setGlavniGlumac("Zac Efron");
		f2.setStanje(12);
		f2.setCena(200.0);
		f2.setZanr(z3);
		f2.setBioskop(b1);
		filmService.save(f2);
		
		Film f3 = new Film();
		f3.setNaziv("Titanik");
		f3.setGlavniGlumac("Leonardo DiCaprio");
		f3.setStanje(5);
		f3.setCena(400.0);
		f3.setZanr(z1);
		f3.setBioskop(b2);
		filmService.save(f3);
	}
}
