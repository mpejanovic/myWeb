package jwd.projekat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class AppBioskop extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AppBioskop.class, args);

	}

}
