package jwd.projekat.web.dto;



public class FilmDTO {
	private Long id;

	private String naziv;
	private String glavniGlumac;
	private Integer stanje;
	private Double cena;
	private Long bioskopId;
	private String bioskopNaziv;
	private Long zanrId;
	private String zanrNaziv;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getGlavniGlumac() {
		return glavniGlumac;
	}
	public void setGlavniGlumac(String glavniGlumac) {
		this.glavniGlumac = glavniGlumac;
	}
	public Integer getStanje() {
		return stanje;
	}
	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public Long getBioskopId() {
		return bioskopId;
	}
	public void setBioskopId(Long bioskopId) {
		this.bioskopId = bioskopId;
	}
	public String getBioskopNaziv() {
		return bioskopNaziv;
	}
	public void setBioskopNaziv(String bioskopNaziv) {
		this.bioskopNaziv = bioskopNaziv;
	}
	public Long getZanrId() {
		return zanrId;
	}
	public void setZanrId(Long zanrId) {
		this.zanrId = zanrId;
	}
	public String getZanrNaziv() {
		return zanrNaziv;
	}
	public void setZanrNaziv(String zanrNaziv) {
		this.zanrNaziv = zanrNaziv;
	}
	
	
	
	

}
