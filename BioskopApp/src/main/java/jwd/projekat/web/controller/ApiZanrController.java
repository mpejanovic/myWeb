package jwd.projekat.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.projekat.model.Film;
import jwd.projekat.model.Zanr;
import jwd.projekat.service.FilmService;
import jwd.projekat.service.ZanrService;
import jwd.projekat.support.FilmToFilmDTO;
import jwd.projekat.support.ZanrToZanrDTO;
import jwd.projekat.web.dto.FilmDTO;
import jwd.projekat.web.dto.ZanrDTO;

@RestController
@RequestMapping(value="/api/zanrovi") //RADI SVE
public class ApiZanrController {
	
	@Autowired
	ZanrService zanrService;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	ZanrToZanrDTO toDto;
	
	@Autowired
	FilmToFilmDTO toFilmDTO;
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<List<ZanrDTO>> getAll(){
		List<Zanr> lista = zanrService.findAll();
		return new ResponseEntity<>(toDto.convert(lista), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}/filmovi")
	ResponseEntity<List<FilmDTO>> getAllFilmovi(@PathVariable Long id){
		List<Film> findAll = filmService.findByZanrId(id);
		return new ResponseEntity<>(toFilmDTO.convert(findAll), HttpStatus.OK);
	}
}
