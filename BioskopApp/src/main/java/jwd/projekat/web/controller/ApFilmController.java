package jwd.projekat.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.projekat.model.Film;
import jwd.projekat.service.FilmService;
import jwd.projekat.support.FilmDTOToFilm;
import jwd.projekat.support.FilmToFilmDTO;
import jwd.projekat.web.dto.FilmDTO;

@RestController
@RequestMapping(value="/api/filmovi") //RADI SVE
public class ApFilmController {
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	FilmToFilmDTO toFilmDto;
	
	@Autowired
	FilmDTOToFilm toFilm;
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<List<FilmDTO>> getAll(@RequestParam(required=false) String naziv, @RequestParam(required=false) Double min, @RequestParam(required=false) Double max, @RequestParam(required=false) Long bioskopId, @RequestParam(required=false) Long zanrId,@RequestParam(required=false) Integer stanje,@RequestParam(defaultValue="0") int page){
		Page<Film> film;
		HttpHeaders headers = new HttpHeaders();
		if(naziv != null || min != null || max != null || bioskopId != null || zanrId != null || stanje != null) {
			film = filmService.pretraga(naziv, min, max, bioskopId, zanrId, stanje, page);
		} else {
			film = filmService.findAll(page);
		}
//		int brSati = 0;
//		for(int i = 0; i < piva.getNumberOfElements(); i++) {
//			System.out.println(piva.getNumberOfElements());
//			brSati += piva.getContent().get(i).getSati();
//		}
//		headers.add("brojSati", Integer.toString(brSati));
		
		headers.add("totalPages", Integer.toString(film.getTotalPages()));
		return new ResponseEntity<>(toFilmDto.convert(film.getContent()), headers, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	ResponseEntity<FilmDTO> getOne(@PathVariable Long id){
		Film film = filmService.findOne(id);
		if(film == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(toFilmDto.convert(film), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	ResponseEntity<FilmDTO> delete(@PathVariable Long id){
		Film film = filmService.findOne(id);
		if(film == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		filmService.delete(id);
		return new ResponseEntity<>(toFilmDto.convert(film), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	ResponseEntity<FilmDTO> edit(@PathVariable Long id,@Validated @RequestBody FilmDTO dto){
		if(!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Film film = toFilm.convert(dto);
		Film izmenjen = filmService.save(film);
		return new ResponseEntity<>(toFilmDto.convert(izmenjen), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<FilmDTO> edit(@Validated @RequestBody FilmDTO dto){
		Film film = toFilm.convert(dto);
		Film sacuvan = filmService.save(film);
		return new ResponseEntity<>(toFilmDto.convert(sacuvan), HttpStatus.CREATED);
	}
	//KUPOVINA
	@RequestMapping(method=RequestMethod.POST, value="/{id}/kupi")
	ResponseEntity<FilmDTO> kupi(@PathVariable Long id){
		
		Film film = filmService.kupi(id);
		if(film == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Film toSave = filmService.save(film);
		return new ResponseEntity<>(toFilmDto.convert(toSave), HttpStatus.OK);
	}
	
//	//ResponseEntity<ZadatakDTO> editStanje(@PathVariable Long id, @RequestBody ZadatakDTO dto){
//	@RequestMapping(method=RequestMethod.POST, value="/{id}/stanja")
//	ResponseEntity<PivoDTO> editStanje(@PathVariable Long id){
//		Pivo zadatak = pivoService.findOne(id);
//		if(zadatak == null) {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//		Pivo zad = pivoService.promeniStanje(zadatak.getId());
//		Pivo izmenjen = pivoService.save(zad);
//		return new ResponseEntity<>(toPivoDto.convert(izmenjen), HttpStatus.OK);
//	}
	
	@ExceptionHandler
	public ResponseEntity<Void> validationHandler(
					DataIntegrityViolationException e){
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
