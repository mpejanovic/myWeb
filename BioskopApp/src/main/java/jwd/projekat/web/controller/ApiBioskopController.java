package jwd.projekat.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import jwd.projekat.model.Bioskop;
import jwd.projekat.model.Film;
import jwd.projekat.service.BioskopService;
import jwd.projekat.service.FilmService;
import jwd.projekat.support.BioskopToBioskopDTO;
import jwd.projekat.support.FilmToFilmDTO;
import jwd.projekat.web.dto.BioskopDTO;
import jwd.projekat.web.dto.FilmDTO;

@RestController
@RequestMapping(value="/api/bioskopi")   //SVE RADI
public class ApiBioskopController {
	
	@Autowired
	BioskopService bioskopService;
	
	@Autowired
	BioskopToBioskopDTO toBioskopDto;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	FilmToFilmDTO toFilmDTO;
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<List<BioskopDTO>> getAll(){
		List<Bioskop> lista = bioskopService.findAll();
		return new ResponseEntity<>(toBioskopDto.convert(lista), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	ResponseEntity<BioskopDTO> getOne(@PathVariable Long id){
		Bioskop bioskop = bioskopService.findOne(id);
		if(bioskop == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(toBioskopDto.convert(bioskop), HttpStatus.OK);
	}
	

	@RequestMapping(method=RequestMethod.GET, value="/{id}/filmovi")
	ResponseEntity<List<FilmDTO>> getAllFilmovi(@PathVariable Long id){
		List<Film> findAll = filmService.findByBioskopId(id);
		return new ResponseEntity<>(toFilmDTO.convert(findAll), HttpStatus.OK);
	}
//	//isto kao gore ali paginirano
//	@RequestMapping(value="/{mestoId}/festivali")
//	public ResponseEntity<List<FestivalDTO>> festivaliMesta(
//			@PathVariable Long mestoId,
//			@RequestParam(defaultValue="0") int pageNum){
//		Page<Festival> festivali = festivalService.findByMestoId(pageNum, mestoId);
//		
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("totalPages", Integer.toString(festivali.getTotalPages()) );
//		return  new ResponseEntity<>(
//				toFestivalDTO.convert(festivali.getContent()),
//				headers,
//				HttpStatus.OK);
//	}
}
